package ru.psu.movs.trrp.socketmq;

import java.sql.*;

public class NonnormalizeToNormalizeParser {
    static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    static final String USER = "postgres";
    static final String PASS = "163252WH";

    public NonnormalizeToNormalizeParser() { }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, USER, PASS);
    }
    public PreparedStatement CreateFacultySelect(Connection connection, String facultyName) throws SQLException {
        PreparedStatement cmdSelect = connection.prepareStatement("SELECT id FROM faculty WHERE name = ?");
        cmdSelect.setString(1, facultyName);
        return cmdSelect;
    }

    public PreparedStatement CreateFacultyInsert(Connection connection, String facultyName) throws SQLException {
        PreparedStatement cmdInsert = connection.prepareStatement("INSERT INTO faculty (name) VALUES (?) RETURNING id");
        cmdInsert.setString(1, facultyName);
        return cmdInsert;
    }

    public PreparedStatement CreateSubjectSelect(Connection connection, String subjectName) throws SQLException {
        PreparedStatement cmdSelect = connection.prepareStatement("SELECT id FROM subject WHERE name = ?");
        cmdSelect.setString(1, subjectName);
        return cmdSelect;
    }

    public PreparedStatement CreateSubjectInsert(Connection connection, String subjectName) throws SQLException {
        PreparedStatement cmdInsert = connection.prepareStatement("INSERT INTO subject (name) VALUES (?) RETURNING id");
        cmdInsert.setString(1, subjectName);
        return cmdInsert;
    }

    public PreparedStatement CreateGroupSelect(Connection connection, String code) throws SQLException {
        PreparedStatement cmdSelect = connection.prepareStatement("SELECT id FROM student_group WHERE code = ?");
        cmdSelect.setString(1, code);
        return cmdSelect;
    }

    public PreparedStatement CreateGroupInsert(Connection connection,
                                               String code, int facultyId, int studentsCount) throws SQLException {
        PreparedStatement cmdInsert = connection.prepareStatement("INSERT INTO student_group (code, faculty_id, students_count) " +
                "VALUES (?, ?, ?) RETURNING id");
        cmdInsert.setString(1, code);
        cmdInsert.setInt(2, facultyId);
        cmdInsert.setInt(3, studentsCount);
        return cmdInsert;
    }

    public PreparedStatement CreateStudentSelect(Connection connection, int groupId,
                                                 String surname, Boolean isMale, String phone) throws SQLException {
        PreparedStatement cmdSelect = connection.prepareStatement("SELECT id FROM student WHERE group_id = ? AND " +
                "surname = ? AND is_male = ? AND phone = ?");
        cmdSelect.setInt(1, groupId);
        cmdSelect.setString(2, surname);
        cmdSelect.setBoolean(3, isMale);
        cmdSelect.setString(4, phone);
        return cmdSelect;
    }

    public PreparedStatement CreateStudentInsert(Connection connection, int groupId, String surname,
                                                 Boolean isMale, String phone) throws SQLException {
        PreparedStatement cmdInsert = connection.prepareStatement("INSERT INTO student (group_id, surname, is_male, phone) " +
                "VALUES (?, ?, ?, ?) RETURNING id");
        cmdInsert.setInt(1, groupId);
        cmdInsert.setString(2, surname);
        cmdInsert.setBoolean(3, isMale);
        cmdInsert.setString(4, phone);
        return cmdInsert;
    }

    public int FillTableAndGetId(PreparedStatement cmdSelect, PreparedStatement cmdInsert) throws SQLException {
        int id;
        ResultSet rdr = cmdSelect.executeQuery();
        if (rdr.next()) {
            id = rdr.getInt(1);
            rdr.close();
        } else {
            rdr.close();
            rdr = cmdInsert.executeQuery();
            rdr.next();
            id = rdr.getInt(1);
        }
        return id;
    }

    public void FillTableSession(Connection connection, int studentId, int subjectId, int score) throws SQLException {
        PreparedStatement cmdSelect = connection.prepareStatement("SELECT * FROM session WHERE student_id = ? AND " +
                "subject_id = ? AND score = ?");
        cmdSelect.setInt(1, studentId);
        cmdSelect.setInt(2, subjectId);
        cmdSelect.setInt(3, score);
        ResultSet rdr = cmdSelect.executeQuery();
        if (rdr.next()) {
            rdr.close();
        } else {
            rdr.close();
            PreparedStatement insertCmd = connection.prepareStatement("INSERT INTO session VALUES (?, ?, ?)");
            insertCmd.setInt(1, studentId);
            insertCmd.setInt(2, subjectId);
            insertCmd.setInt(3, score);
            insertCmd.executeUpdate();
        }
    }
}