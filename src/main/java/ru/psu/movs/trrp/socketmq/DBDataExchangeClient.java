package ru.psu.movs.trrp.socketmq;

import com.rabbitmq.client.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeoutException;

public class DBDataExchangeClient {
    private final Channel channel;
    private final String SERVER_HOST = "192.168.100.6";
    private final int SERVER_PORT = 25836;
    private final Crypto cryptographer;

    public static void main(String[] args) throws IOException, TimeoutException, NoSuchAlgorithmException {
        new DBDataExchangeClient().start();
    }

    private DBDataExchangeClient() throws IOException, TimeoutException, NoSuchAlgorithmException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("guest");
        factory.setPassword("guest");
        Connection connection = factory.newConnection();
        this.channel = connection.createChannel();

        cryptographer = new Crypto();
    }

    private void start() {
        Set<String> resources = getResources();
        if (resources != null)
            resources.forEach(this::subscribeTo);
    }

    private Set<String> getResources() {
        try (Socket socket = new Socket(SERVER_HOST, SERVER_PORT)) {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(cryptographer.getPublicRSAKey());
            oos.flush();
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            Object receivedObject = ois.readObject();
            if (receivedObject instanceof byte[]) {
                byte[] decryptedDESBytes = cryptographer.decryptionRSA((byte[]) receivedObject);
                SecretKey DESKey = new SecretKeySpec(decryptedDESBytes, "DES");
                cryptographer.setMyDESKey(DESKey);
            }

            receivedObject = ois.readObject();
            if (receivedObject instanceof HashSet<?>) {
                return (HashSet<String>) receivedObject;
            }
        } catch (IOException | ClassNotFoundException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void subscribeTo(String resource) {
        try {
            String queueName = channel.queueDeclare("", false, false, true, null).getQueue();
            System.out.println(queueName);
            try (Socket socket = new Socket(SERVER_HOST, SERVER_PORT)) {
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject(new SubscriptionArgs(resource, queueName));
                oos.flush();
            }
            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                    try {
                        byte[] decryptedBody = cryptographer.decryptionDES(body);
                        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(decryptedBody));
                        DBRecord newRecord = (DBRecord) in.readObject();

                        NonnormalizeToNormalizeParser parser = new NonnormalizeToNormalizeParser();
                        java.sql.Connection connection = parser.getConnection();

                        int facultyId = parser.FillTableAndGetId(parser.CreateFacultySelect(connection, newRecord.getFacultyName()),
                                parser.CreateFacultyInsert(connection, newRecord.getFacultyName()));
                        int subjectId = parser.FillTableAndGetId(parser.CreateSubjectSelect(connection, newRecord.getSubjectName()),
                                parser.CreateSubjectInsert(connection, newRecord.getSubjectName()));
                        int groupId = parser.FillTableAndGetId(parser.CreateGroupSelect(connection, newRecord.getCode()),
                                parser.CreateGroupInsert(connection, newRecord.getCode(), facultyId, newRecord.getStudentsCount()));
                        int studentId = parser.FillTableAndGetId(
                                parser.CreateStudentSelect(connection, groupId, newRecord.getStudentSurname(),
                                        newRecord.getStudentIsMale() == 1, newRecord.getStudentPhone()),
                                parser.CreateStudentInsert(connection, groupId, newRecord.getStudentSurname(),
                                        newRecord.getStudentIsMale() == 1, newRecord.getStudentPhone()));

                        parser.FillTableSession(connection, studentId, subjectId, newRecord.getScore());
                        connection.close();

                        System.out.println("row successfully added!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            channel.basicConsume(queueName, true, consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
