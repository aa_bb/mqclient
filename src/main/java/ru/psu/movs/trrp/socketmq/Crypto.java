package ru.psu.movs.trrp.socketmq;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;

public class Crypto {
    private PrivateKey privateRSAKey;
    private PublicKey publicRSAKey;
    private SecretKey myDESKey;
    private static final byte[] iv = { 11, 22, 33, 44, 99, 88, 77, 66 };
    private final AlgorithmParameterSpec paramSpec;

    public Crypto() throws NoSuchAlgorithmException {
        RSAKeyPairGenerator();
        paramSpec = new IvParameterSpec(iv);
    }

    public void RSAKeyPairGenerator() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair pair = keyGen.generateKeyPair();
        this.privateRSAKey = pair.getPrivate();
        this.publicRSAKey = pair.getPublic();
    }

    public void setMyDESKey(SecretKey myDESKey) {
        this.myDESKey = myDESKey;
    }

    public byte[] decryptionRSA(byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateRSAKey);
        return cipher.doFinal(data);
    }

    public byte[] decryptionDES(byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException,
            IllegalBlockSizeException, InvalidKeyException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, myDESKey, paramSpec);
        return cipher.doFinal(data);
    }

    public PublicKey getPublicRSAKey() {
        return publicRSAKey;
    }
}
