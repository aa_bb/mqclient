package ru.psu.movs.trrp.socketmq;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class DBRecord implements Serializable {
    private String facultyName;
    private int studentsCount;
    private String code;
    private String studentSurname;
    private int studentIsMale;
    private String studentPhone;
    private String subjectName;
    private int score;

    public String getFacultyName() {
        return facultyName;
    }

    public int getStudentsCount() {
        return studentsCount;
    }

    public String getCode() {
        return code;
    }

    public String getStudentSurname() {
        return studentSurname;
    }

    public int getStudentIsMale() {
        return studentIsMale;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public int getScore() {
        return score;
    }

    public DBRecord(String facultyName, int studentsCount, String code, String studentSurname,
                    int studentIsMale, String studentPhone, String subjectName, int score) {
        this.facultyName = facultyName;
        this.studentsCount = studentsCount;
        this.code = code;
        this.studentSurname = studentSurname;
        this.studentIsMale = studentIsMale;
        this.studentPhone = studentPhone;
        this.subjectName = subjectName;
        this.score = score;
    }

    public byte[] toByteArray() {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(this);
            out.flush();
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
