package ru.psu.movs.trrp.socketmq;

import java.io.Serializable;

class SubscriptionArgs implements Serializable {
    final String resource;
    final String queueName;

    SubscriptionArgs(String resource, String queueName) {
        this.resource = resource;
        this.queueName = queueName;
    }
}
