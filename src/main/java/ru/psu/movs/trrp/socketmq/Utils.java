package ru.psu.movs.trrp.socketmq;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.UUID;

class Utils {
    private static final String exchangePrefix = UUID.randomUUID().toString();

    private static String encodeResourceUrl(String resource) {
        String encodedResourceUrl;
        try {
            encodedResourceUrl = URLEncoder.encode(resource, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            encodedResourceUrl = Base64.getEncoder().encodeToString(resource.getBytes());
        }
        return encodedResourceUrl;
    }

    static String buildExchangeName(String resource) {
        return exchangePrefix + "" + encodeResourceUrl(resource);
    }
}
